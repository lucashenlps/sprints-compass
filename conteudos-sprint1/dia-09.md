![nono-dia](https://i.ibb.co/F0F0Xz4/Nono-Dia.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Infraestrutura global da AWS

Conformidade: necessidade de executar os dados em uma área específica

Por exemplo, se sua empresa exige que todos os dados residam dentro dos limites do Reino Unido, você deve escolher a Região Londres.

Escolher a região com base na proximidade com localização dos clientes

As vezes a região mais próxima pode não ter todos os recursos que você deseja oferecer aos clientes.

Cada região tem uma tabela de preço diferente

## Zona de disponibilidade

Uma Zona de Disponibilidade é um único data center ou um grupo de data centers em uma Região. Se ocorrer um desastre em uma parte da Região, há distância suficiente para reduzir a chance de que várias Zonas de Disponibilidade sejam afetadas.

## Sub-redes

Uma sub-rede é uma seção de uma VPC na qual você pode agrupar recursos com base em necessidades operacionais ou de segurança. 

Sub-redes públicas contêm recursos que precisam ser acessíveis ao público, como o site de uma loja on-line.

As sub-redes privadas contêm recursos que devem ser acessíveis apenas pela sua rede privada, como um banco de dados contendo informações pessoais dos clientes e históricos de pedidos.

## Domain Name System (DNS)

Você pode pensar no DNS como sendo a lista telefônica da internet. A resolução de DNS é o processo de conversão de um nome de domínio para um endereço IP. 

![exemplo-dns](https://i.ibb.co/8DPMJNS/imagem-2023-08-15-160322802.png)

# Armazenamento e banco de dados

##  Amazon Elastic Block Store (Amazon EBS)

É um serviço que fornece volumes de armazenamento a nível de bloco que você pode usar com instâncias do Amazon EC2.

 Se você interromper ou encerrar uma instância do Amazon EC2, todos os dados no volume do EBS anexo permanecerão disponíveis.

tanto a instância do Amazon EC2 quanto o volume do EBS devem residir na mesma Zona de Disponibilidade.

### Snapshots do Amazon EBS

![amazon-ebs](https://i.ibb.co/W0PYKKr/imagem-2023-08-15-163216078.png)

Um snapshot do EBS é um backup incremental. Isso significa que o primeiro backup de um volume copia todos os dados. Nos backups subsequentes, somente os blocos de dados que foram alterados desde o snapshot mais recente são salvos. 

## Amazon Simple Storage Service (Amazon S3)

É um serviço que fornece armazenamento a nível do objeto. O Amazon S3 armazena dados como objetos em buckets.


Armazenar e recuperar uma quantidade ilimitada de dados

No armazenamento de objetos, cada objeto consiste em dados, metadados e uma chave.

Os **dados** podem ser uma imagem, vídeo, documento de texto ou qualquer outro tipo de arquivo. Os **metadados** contêm informações sobre o que são os dados, como eles são usados, o tamanho do objeto, etc. A **chave** de um objeto é seu identificador exclusivo.

### S3 Standard

Dados acessados com frequência com armazenamento em um mínimo de três **Zonas de Disponibilidade**

### S3 Standard-IA

Dados acessados com baixa frequência, preço de armazenamento mais baixo e preço de recuperação mais alto

### S3 One Zone - IA

Armazenamento de dados em **uma única** Zona de Disponibilidade e com preço menor do que o citado anteriormente

Bom caso você possa reproduzir seus dados em caso de falha na Zona de Disponibilidade

### S3 Intelligent-Tiering

Aloca os objetos para algum dos planos acima baseado em sua taxa de acesso: baixo acesso -> S3 Standard - IA, frequente acesso -> S3 Standard

### S3 Glacier

Categoria de armazenamento de baixo custo, ideal para o arquivamento de dados. **Recupera objetos em poucos minutos a horas**

### S3 Glacier Deep Archive

Armazenamento de objetos com o menor custo, recupera objetos em até 12 horas.

 Ao decidir entre o Amazon S3 Glacier e o Amazon S3 Glacier Deep Archive, considere a prontidão com que você precisa recuperar objetos arquivados.

## Amazon Elastic File System (Amazon EFS) 

Sistema de arquivos escalável(nuvem AWS e recursos locais). 

À medida que você adiciona e remove arquivos, o Amazon EFS expande e retrai automaticamente.
    
Armazena dados em várias Zonas de Disponibilidade e entre elas.   

## Bancos de dados relacionais

Em um banco de dados relacional, os dados são armazenados de forma que se relacionem a outras partes de dados.

### Amazon Relational Database Service

Serviço que permite executar bancos de dados relacionais na nuvem AWS, automatiza tarefas como provisionamento de hardware, configuração de banco de dados, patch e backups.

 *Você pode integrar o Amazon RDS a outros serviços para atender às suas necessidades de negócios e operacionais, como usar o AWS Lambda para consultar seu banco de dados a partir de um aplicativo sem servidor.*

#### Amazon Aurora

É até cinco vezes mais rápido do que os bancos de dados MySQL comuns e até três vezes mais rápido do que os bancos de dados PostgreSQL comuns.

Ele replica seis cópias de seus dados em três Zonas de Disponibilidade e faz backup contínuo de seus dados para o Amazon S3.

## Bancos de dados não relacional

Em um banco de dados não relacional, você cria tabelas. Uma tabela é um lugar onde você pode armazenar e consultar dados. Usam estruturas diferentes de linhas e colunas para organizar dados.

os dados são organizados em itens (chaves) e cada item tem um atributo (valores).

você pode adicionar ou remover atributos de itens na tabela a qualquer momento. Além disso, nem todos os itens na tabela precisam ter os mesmos atributos. 

![no-sql-db](https://i.ibb.co/f9ShTGD/imagem-2023-08-15-172229383.png)

### Amazon DynamoDB

Serverless, ou seja, não precisa provisonar, aplicar patches ou gerenciar servidores

Ajuste automático baseado nas alterações de capacidade (auto scaling)





