![quinto-dia](https://i.ibb.co/55V40L1/Quinto-Dia-1.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Tipos de Testes Baseados na IEC/ISO 25010

## Adequação Funcional

Diz respeito sobre se determinada funcionalidade está de acordo com o que foi pedido

• Completude -> Se determinada funcionalidade está completa

• Correção -> Verificar se o resultado entregue está correto (Exemplo: Numa calculadora, efetuar a soma de 2 mais 3, retornando 5 como produto)

• Apropriado -> Se o resultado é apropriado a situação (Exemplo: Numa mesma calculadora, efetuar a soma de 2 mais 3, porém retorna V como resultado, isso implica que o programa está **completo** (há todas as etapas), calcula corretamente a equação (2 + 3 = 5, no caso V), porém, nesse contexto **não** é o ideal exibir a informação dessa forma)

## Usabilidade

É o grau de facilidade que o usuário enfrenta ao utilizar o produto. Quanto menos o usuário precisar de treinamento, representa maior usabilidade daquele software. Se divide em 6 subcaracterísticas, são elas:

• Reconhecibilidade
-> Facilitar que o usuário reconheça elementos e comportamentos

• Aprendizibilidade
-> Facilitar o aprendizado do usuário

• Operabilidade
-> Facilitar a operação e navegação (menos cliques; menos tempo)

• Proteção Contra Erro do Usuário
-> Não permitir o usuário de fazer uma escolha indevida, podendo comprometer o funcionamento do software

• Estética (da Interface do Usuário)

• Acessibilidade
-> Facilitar o acesso a todas as pessoas

## Compatibilidade

Saber se um software é compatível com outros softwares

• Coexistência
-> Facilidade de coexistir

• Interoperabilidade
-> Facilidade de comunicar com outros softwares

## Confiabilidade

A certeza de que determinado software está sempre disponível ao seu uso

• Maturidade
-> Perceber e prevenir a falha antes que aconteça

• Disponibilidade
-> Manter-se a disposição de usuários e sistemas 

• Tolerância a Falhas
-> Perceber e compensar as falhas em tempo real

• Recuperabilidade
-> Recuperar-se de falhas e travamentos

## Eficiência (de Desempenho)

Verificar se o software é rápido para executar suas tarefas

• Comportamento em Relação ao Tempo
-> Performance, desempenho

• Utilização de Recursos
-> Análise de como ele faz uso da memória RAM, disco, processador, etc.

• Capacidade (de Atender Transações e Usuários)
-> Suportar grandes fluxos de movimentações em dias específicos (Black Friday, Dias dos Namorados, etc.)

## Manutenibilidade

 Facilidade de dar manutenção a um software

 • Modularidade
 -> Organizado em módulos; Maior facilidade para conserto, modificação ou reposição, visto que eles não estão anexados como um todo

 • Reusabilidade
 -> Facilidade de reutilizar; Criar de maneira mais genérica e especializar o uso de determinada função quando for necessário; Será utilizado por várias pessoas

 • Analisabilidade
 -> Facilidade de Analisar; Ser construído de modo que favoreça seu entendimento; Alterar, modificar o software **com** a necessidade de codificar

 • Modificabilidade
 -> Facilidade de Modificar; Troca de componentes; Alterar, modificar o software **sem** a necessidade de codificar

 • Testabilidade
 -> Facilidade de Testar

## Portabilidade

Capacidade de um software de funcionar em vários sistemas operacionais

• Adaptabilidade
-> Facilidade de Adaptar (Facilidade de funcionar em múltiplos ambientes)

• Instalabilidade
-> Facilidade de Instalar e Desinstalar

• Substituibilidade
-> Facilidade de Substituir (um software anterior por um novo)

## Segurança 

 • Confidencialidade
 -> Somente quem criou aquela informação ou quem possui maior posição na hierarquia, pode acessá-la
 
 • Integridade
 -> Somente pessoas autorizadas podem modificar as informações. Se houver alguma mudança, deve registrar **quem** mudou e **quando**

 • Não Repúdio
 -> Garantir que a pessoa que faz a transação é realmente usuário/cliente do software

 • Responsabilidade
 -> Log para comprovar que determinada pessoa que utilizou o software fez tal ação. Permite prestar contas sobre as atividades dos usuários

 • Autenticidade
 -> Garantir que determinada transação foi feita 

 # Testes Manuais x Testes Automatizados

 ## Testes Automatizados

 • Um processo de construção e teste automatizado ocorre em uma base diária e detecta erros de interação de modo antecipado e rápido

 • A integração contínua permite que os testadores ágeis realizem testes automatizados regularmente, em alguns casos, como parte do próprio processo de integração contínua, e enviar feedback rápido para a equipe sobre a qualidade do código
 
 • Os resultados são visíveis para todos os membros da equipe, especialmente quando os relatóros automatizados são integrados no processo

• Podem ser contínuos ao longo da iteração

• Abrangem a maior quantidade de funcionalidades possíveis incluindo estórias de usuários desenvolvidos nas iterações anteriores

• Boa cobertura nos testes de regressão automatizados ajuda no desenvolvimento (e teste) de grandes sistemas integrados

• Quando o teste de regressão é automatizado, os testadores ágeis são livres para concentrar seus testes manuais em:

-> Novas Funcionalidades

-> Mudanças Implementadas

-> Teste de Confirmação (Reteste de defeitos corrigidos)

# Estático x Dinâmico

## Teste Dinâmico

• Necessita que o software seja executado

• É o mais utilizado pelo mercado

• Custo tende a ser mais alto

## Teste Estático

• Revisão, inspeção e análise estática dos artefatos

• Qualquer documento do projeto pode ser avaliado desta forma

# Teste e Depuração

## Teste

A execução dos testes pode mostrar falhas causadas por defeitos no software

• O teste só aumenta a qualidade do software quando:

-> Defeitos são **encontrados** e **corrigidos**

-> Ocorre a verificação da conformidade dos requisitos **funcionais**

-> Requisitos **não-funcionais**(confiabilidade, usabilidade, escalabilidade, etc) são verificados

• A qualidade custa **menos**!

-> Reduzimos drasticamente o retrabalho

-> Reduzimos custos de manutenção e os chamados de suporte

-> Obtemos um software bem estruturado que facilita novos projetos

Mais qualidade = Produção mais rápida = Mais produtividade

## Depuração do Código

É a atividade de desenvolvimento que localiza, analisa e corrige esses defeitos

## Testes de Confirmação

Verificam se as correções resolveram os defeitos

**Por via de regra, o mais usual é que o testador foque nas atividades de 'teste' e 'teste de confirmação', enquanto o desenvolvedor fique com a responsabilidade de depurar o código**







