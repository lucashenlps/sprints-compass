![sétimo-dia](https://i.ibb.co/L1v6hTN/S-timo-Dia.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Funções da TI clássica

## Arquiteto

Fazem o intermédio entre o especialista em TI com líderes de empresas. Para isso ele precisa de conhecimento de ambas as áreas e gerenciar equipes de sua organização.

## Administrador de sistemas: função

Dão suporte à instalação e configuração de hardware e sistemas operacionais, cobrindo reparos e substituição de equipamento, baseado em como está sua performance.
Monitoramento de rede e gerenciamento dos arquivos de log e controle de acesso dos usuários também fazem parte de seu papel como profissional.

## Administrador de aplicações: função

Solucionam os problemas de uma aplicação, podendo necessitar da colaboração dos desenvolvedores e fornecedores do produto. Garantem que haja a integração correta do aplicativo.

## Administrador de banco de dados: função

Instalam e cuidam da manutenção de DB no ambiente, treinam funcionários para usarem o banco de dados corretamente

## Administrador de rede: função

Responsável pelo projeto, instalação, configuração e manutenção de LANs e WANs

Cuidam de tudo que envolve rede, desde pensar quais tipos de redes são melhores para a empresa, instalar e configurar os hardwares de rede juntamente com seus dispositivos que se comunicam com ela, adição de novos dispositivos de rede, configuração de serviços e solução de problemas.
Também possuem papel de expandir a estrutura de rede conforme o crescimento da empresa.

## Administrador de armazenamento: função

É responsável por gerenciar os sistemas de armazenamento no ambiente de TI e sua manutenção. A manutenção também requer testes dos sistemas de armazenamento, incluem testes de integração e testes de estresse.
Um administrador de armazenamento trabalhará com um administrador de aplicações para fornecer suporte às aplicações quando houver potenciais problemas de armazenamento

## Administrador de segurança: função

Responsável por instalar, administrar e solucionar problemas de soluções e controles de segurança de uma organização. Eles analisam e impõem os requisitos de segurança de TI

Ajudam a se defender contra acesso não autorizado, a modificação e destruição dos dados e o acesso aos dados.
Configuram e usam ferramentas de suporte de segurança, como firewalls e software antivírus.
Executam testes de penetração da rede e de vulnerabilidades



# Funções na nuvem

Existem quatro esferas de responsabilidade para a gestão empresarial. O arquiteto enterprise(empresarial) tem responsabilidades em **todas** as esferas.

Na esfera de **gerenciamento da empresa**, trabalha com o **gerente de programa** e **gerente financeiro**. Também trabalha com arquitetos nas outras esferas para projetar as arquiteturas gerais para a empresa

O **arquiteto de infraestrutura de nuvem** projeta arquiteturas de infraestrutura de nuvem dependentes de soluções criadas pelo engenheiro de operações de infraestrutura de nuvem.

O **arquiteto de segurança** especifica os requisitos de segurança que o engenheiro de operações de segurança gerencia, monitora e impõe

O **arquiteto de aplicações** projeta aplicações otimizadas para a nuvem, que são desenvolvidas pelo desenvolvedor de aplicações e criadas e automatizadas pelo engenheiro de DevOps

A plataforma de nuvem gera valor para uma empresa hospedando uma variedade de aplicações e dados empresariais.

O Centro de excelência em nuvem consiste no escritório empresarial em nuvem. O escritorio da nuvem integra as pessoas na plataforma de nuvem e de marketing e treina novos funcionários em todos os padrões de nuvem necessários. 

# Serviços comuns na nuvem

Existem muitos serviços na nuvem. Esta tabela mostra alguns desses serviços e as funções usadas para executá-los

![serviços-nuvem](https://i.ibb.co/hgFzFyK/imagem-2023-08-11-160403564.png)

Os serviços também podem ser executados por várias funções

# Função na nuvem: arquiteto enterprise de nuvem

Eles são responsáveis por **todo o ambiente**. 

Eles estão envolvidos em todo processo de desenvolvimento. Um arquiteto enterprise de nuvem **traduz os requisitos de negócio** e os casos de uso em recursos verificados para **atender** os requisitos.

O arquiteto fornece e mantém modelos como uma estrutura de trabalho para orientar o que deve ser feito dentro ou fora da organização. 

# Função na nuvem: gerente de programa

Atribuem e gerenciam funções operacionais relacionadas ao desenvolvimento e gerenciamento de uma aplicação.  
Também gerenciam e monitoram as métricas da nuvem relacionadas à integridade operacional da nuvem como um todo. Um bom exemplo seria a latência entre usuários finais e as aplicações de nuvem. 

# Função na nuvem: gerente financeiro

São encarregados da distribuição do custo dos recursos de nuvem para as organizações que as usam. 
Por exemplo, com a AWS, um gerente financeiro deve fornecer orientações sobre quando uma empresa deve usar as instâncias de reserva, as instâncias spot ou as instâncias sob demanda do Amazon EC2

# Função na nuvem: arquiteto da infraestrutura de nuvem

Projetam arquiteturas de infraestrutura de nuvem dependentes de soluções com base nos requisitos de negócio. Trabalha com especialistas em tecnologia para projetar as arquiteturas de infraestrutura de nuvem apropriadas.

# Função na nuvem: engenheiro de operações em nuvem

 Garantem que a infraestrutura de nuvem atenda aos requisitos de serviços

# Função na nuvem: engenheiro de operações em nuvem - gerenciamento

Eles garantem que a capacidade adequada da infraestrutura virtual esteja disponível para as aplicações. Criam e gerenciam redes virtuais e conectividade de rede.

# Função na nuvem: engenheiro de operações em nuvem - suporte

Os engenheiros de operações em nuvem respondem a incidentes e os encaminham para as equipes apropriadas.
Fornecem suporte quando ocorrem problemas. Fazem ajustes na performance quando necessário. Garantem suporte de backup e recuperação para recursos de nuvem.

# Função na nuvem: arquiteto de segurança em nuvem

Definem os requisitos de segurança que a nuvem deve cumprir. 

# Função na nuvem: engenheiro de operações de segurança

Gerenciam, monitoram e aplicam a infraestrutura de nuvem e a segurança das aplicações, baseado nos requisitos de segurança. Certificam e aprovam novos modelos que atendem aos requisitos de segurança. 

# Função na nuvem: arquiteto de aplicações

Responsável por projetar aplicações otimizadas para a nuvem, eles: 

• Colaboram com outras pessoas

• Executam os requisitos de capacidade e escalabilidade

• Fornecem conhecimento profundo sobre o software

# Função na nuvem: desenvolvedor de aplicações

São responsáveis pelo desenvolvimento de código. Gerenciam a liberação de código no ambiente de nuvem e gerenciam a implantação de código na infraestrutura virtual e nos serviços de plataforma. Ajudam a criar e gerenciar a documentação da aplicação

# Função na nuvem: engenheiro de DevOps

Implantam e configuram compilações diárias e solucionando problemas de compilações com falhas. Se comunicam com frequência com os desenvolvedores e gerentes do projeto. Projetam e criam soluções de automação para o ambiente, voltadas aos sistemas de gerenciamento de configuração, implantação, pipeline, e fonte de aplicações.

# Funções na nuvem e infraestrutura como código

## Gerenciamento manual e automatizado

Configure manualmente seu ambiente usando o Console de Gerenciamento de AWS, a CLI da AWS e as APIs da AWS. Você pode automatizar muitas das tarefas usando infraestrutura como código e o AWS CloudFormation, que usa modelos armazenados em um sistema de controle de código. 

## Por que usar a infraestrutura como código?

Consiste em a infraestrutura ser fornecida e gerenciada usando técnicas de desenvolvimento de código e software, como o versionamento e a integração e a distribuição contínuas

• Codificação de design

• Iteração rápida de projetos

• Manutenção fácil

• Facilidade na adição das práticas de segurança recomendadas pela empresa

## Uso do modelo de DevOps para desenvolver aplicações

Ele cria, compila e testa o código e reúne a aplicação a ser implantada. Valida as imagens e automatiza a implantação da infraestrutura de nuvem

