![segundo-dia](https://i.ibb.co/QCptjFR/Segundo-Dia.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 
# Gestão de Projetos Tradicional
Define tudo o que será feito no projeto em seu **início**, o cliente tem contato com o produto apenas ao estar pronto

Previsão de tudo que será realizado do início ao fim do projeto. Envolve muita documentação, o que resulta numa grande resistência à mudanças a partir de quando o desenvolvimento do projeto entrou em andamento

![gestão tradicional](https://i.ibb.co/dWZ7WFZ/imagem-2023-08-03-214251354.png)

# Gestão de Projetos Ágil
Reuniões de tempos em tempos (geralmente período de 2 a 4 semanas) com o cliente, entregando partes já concluídas do projeto para que haja certeza de como serão os próximos passos

Entrega **frequente** de valor, possibilitando avaliação do cliente **DURANTE** o andamento do projeto, tendo total liberdade para mudar seu desenvolvimento

![principais diferenças](https://i.ibb.co/LYvKS2Q/diferencas.png)

Prioriza a **satisfação do cliente** ao invés de seguir um plano imutável

### Lançamento / Release
Entregas parciais são chamadas de lançamento. Normalmente a cada 1 a 3 _sprints_ *concluídas*, um lançamento é feito, disponibilizando um sub-produto para que o cliente possa fazer uso e dar _feedback_ à empresa

# Qual é a melhor?
![imagem-balança](https://media.gettyimages.com/id/172950142/pt/foto/balança-da-justiça.jpg?s=612x612&w=gi&k=20&c=n_1Q28mR49J4LwLP81Qk549SgwxCUKGfz-19JLpgf4M=)

Se você acredita que há uma gestão de projetos melhor que a outra, você está equivocado.
Cada uma tem seus propósitos e particularidades. É de sua responsabilidade analisar o contexto do projeto e aplicar o que mais cumpre com o necessário

# No que consiste o SCRUM?
![fluxo scrum](https://i.ibb.co/9gk7yjF/imagem-2023-08-03-213701756.png)

# Pilares do _SCRUM_
   • Transparência: O processo emergente e o trabalho devem ser visíveis tanto para quem executa o trabalho quanto para quem recebe trabalho.

   • Inspeção: O progresso em direção às metas acordadas devem ser inspecionadas com **frequência** e **diligência** a fim de detectar variações ou problemas potencialmente indesejáveis

   • Adaptação: Caso o produto resultante for inaceitável, o processo que está sendo aplicado ou os materiais que estão sendo produzidos devem ser ajustados

# Funções no _SCRUM_
## Dono do Produto (_Project Owner_)
Seu papel dentro de um ambiente ágil é **representar o cliente** para a equipe que está desenvolvendo o produto

## _Scrum Master_
Especialista em _SCRUM_. Responsável por fazer com que o SCRUM seja executado adequadamente na empresa 

## Desenvolvedores
Responsáveis por realizar as atividades para gerar um incremento do produto, além de estimarem os itens que vão para a _backlog_ da _Sprint_, juntamente com auxílio do _Project Owner_, baseado na prioridade de cada _User Story_

# Backlog do produto
É a lista do que precisa ser feito para que o produto final fique pronto. Esses requisitos também são chamados de história do usuário e são ordenados por prioridade
# Sprint Planning
É a reunião responsável pela priorização das atividades que serão executadas dentro da _Sprint_ recém iniciada. Envolve também a meta da _Sprint_
# _Daily SCRUM_
Reuniões diárias que consistem em 3 perguntas:
1. O que você **fez** ontem?
2. O que você **fará** hoje?
3. Tem algum **impedimento**?

Possibilitam maior organização entre a equipe e a eliminação de barreiras que podem comprometer a entregue de resultados na _Sprint_ atual
# Fim da Sprint
Ao término da Sprint, ocorre entrega dos produtos que foram determinados anteriormente em uma _Sprint Planning_
Caso o cliente não aprove alguma das tarefas entregues, essa tarefa retorna 
ao _backlog_ do produto, para que possa ser escolhida em _Sprints_ futuras

# Retrospectiva da _Sprint_
A cada fim de _Sprint_ são reavaliados os acertos e erros, para que obtenha melhora no desempenho e minimização de falhas

## Vantagens do SCRUM
Existem muitas vantagens ao trabalharmos utilizando a metodologia ágil, podemos conferir algumas delas abaixo:

• Ambiente propício para um forte trabalho em equipe

• Produtividade extrema, foco e motivação graças ao trabalho em timeboxing

• Rápido _feedback_, flexível a mudanças e com uma resposta imediata
 

