![terceiro-dia](https://i.ibb.co/9Zv9Z6j/Terceiro-Dia.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 
## Por que o próprio Dev não testa?
O desenvolvedor pode e deve testar sim seu programa, existem erros que somente o próprio programador é capaz de encontrar. Segundo Glenford Myers, 50% dos erros é encontrado pelo desenvolvedor responsável

Porém, há erros que são muito difíceis de serem encontrados, isso se dá devido ao Viés de Confirmação, característica humana que dificulta a análise crítica do programa desenvolvido por você mesmo, sendo necessária o teste de outras pessoas

## Quem pode testar?
Todo mundo pode testar, desde PO, Scrum Master, Analistas independente da área de atuação, até a qualquer outra pessoa, cada indivíduo tem sua experiência e percepção única que contribuem com o ato

# Soft Skills Importantes
## Habilidades Pessoais
• Motivação

• Persistência

• Curiosidade

• Gostar de Aprender

• Perfeccionista (necessário a visão de como o programa deveria estar para funcionar corretamente, perfeito)

• Detalhista (ser atento a pequenas mudanças e detalhes que acabariam passando por um profissional de outra área ao testar) 

• Resiliência

• Foco em Solução não em Problema

• Organização
 
• Priorização

• Autogerenciado (a capacidade de saber o que fazer, quando fazer, e **quem** sabe fazer, caso precise pedir ajuda para alguém de outro setor para resolver um problema)

## Habilidades Interpessoais

• Ouvir

• Falar

• Postura Corporal

• Ler (interpretar e analisar o que deveria ser feito)

• Escrever (conseguir se comunicar por este meio com clareza e concisão)

• Desenhar

• Interpretar (gestos, outros)

• Não ser só portador de más notícias

