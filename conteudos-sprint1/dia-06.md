![sexto-dia](https://i.ibb.co/rvgyJ2z/Sexto-Dia-2.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Fundamentos do Teste de Software (backend)

## Teste E2E

Simulam o ambiente real. Por exemplo: Preencher formulários da aplicação, clica em botões, etc, verificando se ocorre o que era esperado

### Ciclo de teste E2E

• Cadastrar

• Escolher item

• Calcular frete

• Sacola

• Pagamento

• Finalizar pedido

• Informações do pedido

• Boleto

• Meus pedidos

# Piramide de Testes

Seu papel é definir níveis de testes e te dar uma direção quanto à quantidade de testes que você deveria ter em cada um desses níveis.

![piramide-testes](https://miro.medium.com/v2/resize:fit:1036/1*S0yR438zKtJtEEBldSviFA.png)

No topo da pirâmide, temos os **testes de ponta a ponta** (end to end ou e2e). Consistem em **imitar** o comportamento do usuário final nas nossas aplicações (seja ele uma pessoa, uma api, ou qualquer outro tipo de cliente)

Na base, temos os **testes de unidade**. Consistem em verificar o funcionamento da menor unidade de código testável da nossa aplicação.

Localizado ao meio da pirâmide, temos os **testes de integração**. Baseiam-se em **verificar** se um conjunto de unidades se comporta de maneira correta, porém de forma menos abrangente do que os **testes end to end**

## Testes de ponta a ponta

Simulam o ambiente real, ou seja, sobem a aplicação ou abrem o navegador, preenchem formulários, clicam em botões e, por fim, verificam se aconteceu o que era esperado. Os testes end to end geralmente acontecem em um ambiente controlado (não o de produção) e quem executa as ações é um robo (não o usuário real)

Esses testes são complexos de escrever e costumam demorar um tempo considerável pra rodar. São testes que cobrem apenas os fluxos **principais** da aplicação

## Testes de unidade

Verificam o funcionamento da menor unidade de código testável da nossa aplicação, **independente da interação dela com outras partes do nosso código**
Quando um teste de unidade falha, é possível saber exatamente onde está o problema.  

A unidade geralmente é vista como um método público em uma classe, mas pode ser vista também como um conjunto de classes/métodos/objetos interagindo entre si.

**A unidade vai ser sempre definida como a menor parte testável do seu sistema.**

## Testes de integração

Podemos ter testado duas unidades que interagem entre si separadamente e concluído que ambas então funcionando como esperado. Ainda assim, é possível que as duas unidades não funcionem em conjunto

Os testes de integração testam algumas unidades funcionando em conjunto. Diferente dos testes de ponta a ponta, são testes que testam **funcionalidades**, e não o **sistema como um todo**

• São mais complicados (de fazer e manter) e demorados que os testes de unidade, por testarem uma funcionalidade inteira

• Bem mais simples (de fazer e manter) e rápidos que os testes de ponta a ponta, por testarem uma única funcionalidade de cada vez, sem precisar subir a aplicação inteira




