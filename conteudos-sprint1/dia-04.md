![quarto-dia](https://i.ibb.co/ZXJxXDt/Quarto-Dia-9.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Hard Skills exigidas de um profissional em técnologia

**• Sistema operacional**

  • Windows

  • Linux
    • Android

  • Mac
    • iOS

  • Mainframe
    -> Grandes computadores, capazes de processar quantidades imensas de informação

**• Office**
  • Editor de Texto
    -> Word
 • Planilha Eletrônica
    -> Excel
  • Apresentação
    -> PowerPoint

**• E-mail**
**• Mensagens instantâneas**

**• Idiomas**

**• Internet**

**• Acesso Remoto**
  • TeamViewer
  • LogMeIn

**• Videoconferência**
  • Zoom
  • Teams
  • Meets

**• Segurança**
  • Privacidade
    • O que acesso
    • Onde acesso

  • Fragilidade
    • Senha
    • Conexões
  • Vulnerabilidades

  # Sobre o Teste de QA
  • Planejar -> entender quais os objetivos, quais os riscos e como fazer para que o projeto dê certo

  • Analisar -> quais os melhores testes para executar dentro do tempo e recursos disponíveis

  • Modelar  -> quais testes você **não** pode deixar de fazer. Funções cuja execução é primordial para o funcionamento do projeto 

 ## Execução
 • Teste Manual -> Navega pelo programa livremente, buscando erros de funcionalidades e até mesmo de layout (incapaz de ser automátizado)

 • Teste Automatizado -> Satisfaz o mesmo propósito que o teste manual, porém com menos custos, ideal para testes de entrada e saída(por exemplo, efetuar uma transação)

 É de extrema importância **comunicar os defeitos** que foram encontrados, de uma maneira positiva, explicar, de maneira clara, qual o passo a passo para chegar a determinado erro

# Danos dos Bugs
### Empresas / Organizações

-> Atrasos, Perda de Confiança / Vendas

### Pessoas

-> Constrangimentos, Perda ou Supressão de Direitos, Risco de Vida e Acidentes

### Governos

-> Vulnerabilidade de Informações,  Decisões Estratégicas Incorretas, Derrotas Militares

### Meio Ambiente

-> Alertas Atrasados, Despercídio de Recursos, Poluição

# Os 7 Fundamentos do Teste (ISTQB)

### Teste Demonstra a Presença de Defeitos, Mas Nunca a Sua Ausência

• O teste pode demonstrar a presença de defeitos, mas não pode provar que eles não existem

• Teste reduz a probabilidade que os defeitos permaneçam em um software, mas mesmo se nenhum defeito for encontrado, não prova que ele esteja perfeito

### Teste Exaustivo não é possível

Priorização dos testes baseado naquilo que se usa mais e nos riscos que podem ocorrer no sistema, quais causam maiores danos e prejuízos caso falhe

• Testar tudo (todas as combinações de entrada e pré-condições) não é viável, exceto para casos triviais

• Em vez dos testes exaustivos, **riscos e prioridades** são levados em consideração para dar foco aos esforços de teste

### Teste Antecipado

Regra 10 de Myers -> _"Quanto mais cedo encontramos um defeito, mais barato será sua identificação e correção"_
![10-myers](https://i.ibb.co/z6QztmH/myers.png)

• A atividade de teste deve começar o mais breve possível no ciclo de desenvolvimento do software ou sistema e deve ser focado em objetivos definidos

### Agrupamento de Defeitos

• Um número pequeno de módulos contém a maioria dos defeitos descobertos durante o teste antes de sua entrega ou exibe a maioria das falhas operacionais

• Os bugs estão distribuídos de forma heterogênea. Alguns módulos têm mais defeitos do que outros

### Paradoxo do Pesticida

• Pode ocorrer de um mesmo conjunto de testes repetidos várias vezes não encontrarem novos defeitos após determinado momento

• Para superar este "paradoxo do pesticida", os casos de testes necessitam ser frequentemente revisado e atualizado

• Um conjunto de testes novo e diferente precisa ser escrito para exercitar diferentes partes do software ou sistema com objetivo de aumentar a possibilidade de encontrar mais erros

### Teste Depende do Contexto

Descubra quais são os riscos

• Testes são realizados de forma diferente conforme o contexto

• Exemplo: Software de piloto automático de um avião deve ser testado com **amplitude** e **profundidade** diferentes de um software de um quiosque de informações de um shopping

### A Ilusão da Ausência de Erros

• Encontrar e consertar defeitos não ajuda se o sistema construído não atende às expectativas e necessidades dos usuários

# Teste e Qualidade

-> O teste é focado em avaliar o produto, se atende as necessidades do cliente, se está de acordo com o que foi planejado em reuniões, etc. O testador pode analisar o produto através de um script automatizado.

-> O QA tem foco em trabalhar melhorando o processo de criação e desenvolvimento do produto

• Projetos anteriores devem prover lições aprendidas:

-> Entendimento da causa raiz dos defeitos encontrados

-> Aprimorar os processos

-> Prevenir reincidência de erros

-> Melhorar a qualidade dos sistemas futuros

• Testes devem ser **integrados como uma das atividades** de garantia da qualidade

• Juntamente aos padrões de desenvolvimento, treinamento, análise de defeitos, e outras ações

# Erro -> Defeito -> Falha

• Pessoas cometem **erros**(enganos), que produzem **defeitos**(bugs) no código, em um software ou sistema ou em um documento

• Se um defeito no código for executado, o sistema falhará ao tentar fazer o que deveria (ou, em algumas vezes, o que **não** deveria), causando uma **falha**

• **Nem todos os defeitos causam falhas** (a falha só existe quando o defeito é executado)

• Falhas geram **insatisfação com a qualidade**






