![oitavo-dia](https://i.ibb.co/wLYrxLG/Oitavo-Dia.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Computação em nuvem

 A entrega de recursos de TI sob demanda pela internet com uma definição de preço de pagamento pelo que usa

# Módulo 2 - Amazon Elastic Compute Cloud (Amazon EC2)

    Altamente flexível, Econômico, Rápido
    Alocação múltipla -> compartilhar hardware subjacente

Instância EC2 -> atendem às necessidades dos funcionários
" da mesma forma que a cafeteria tem diferentes tipos de funcionários, a aws tem diferentes tipos de instancias EC2"

Cada tipo de instancia amazon EC2 é agrupada uma família de instâncias

São elas -> uso geral; otimizada para computaçao; otimizada para memoria; computaçao acelerada; otimizada para armazenamento

## Tipos de instâncias do Amazon EC2

### Instâncias de uso geral

-> Equilibram os recursos de computação, memória e rede. Pode ser usadas para diversas cargas de trabalho:

• servidores de aplicativos

• servidores de jogos

• servidores de back-end para aplicativos empresariais

• bancos de dados pequenos e médios

Você pode executar essa instância num cenário aonde você tenha um aplicativo no qual as necessidades de recursos para computação, memória e rede sejam praticamente equivalentes.

### Instâncias otimizadas para computação

São ideais para aplicativos vinculados à computação que se beneficiam de processadores de alto desempenho.

Aplicativos otimizados para computação são ideais para servidores web de alto desempenho, servidores de aplicativos de computação intensiva e servidores de jogos dedicados.

### Instâncias otimizadas para memória

Fornecem desempenho rápido para áreas que processam grandes conjuntos de dados na memória.
Nos casos que você precise que grandes quantidades de dados sejam pré-carregados antes de executar um aplicativo, considere utilizar instâncias otimizadas para memória.

### Instâncias de computação acelerada

Usam aceleradores de hardware, ou coprocessadores, para executar algumas funções de forma mais eficiente do que é possível em um software executado em CPUs.

    Exemplos dessas funções são cálculos de números com vírgula flutuante, processamento de gráficos e correspondência de padrões de dados.

As instâncias de computação acelerada são ideais para cargas de trabalho, como aplicativos gráficos e streaming de jogos e de aplicativos.

### Instâncias otimizadas para armazenamento

Projetadas para cargas de trabalho que exigem alto acesso sequencial de leitura e gravação a grandes conjuntos de dados no armazenamento local.

    Exemplos: sistemas de arquivos distribuídos, aplicativos de data warehouse e sistemas de processamento de transações on-line de alta frequência (OLTP).

Fornecem dezenas de milhares de IOPS (desempenho de um dispositivo baseado em quantas operações diferentes de entrada e saída ele pode executar por segundo)
aleatórias e de baixa latência

## Opções de compra Amazon EC2

### Sob demanda

 Paga apenas **durante** a execução da instância(hora ou segundo)

### Saving Plans

  Preços baixos no uso do EC2, em troca de um vínculo com uma quantidade consistente de uso em USD/h por um período de 1 ou 3 anos

### Instâncias reservadas

Descontos concedidos ao se comprometer com um prazo de 1 ou 3 anos, podendo escolher entre pagamento adiantado parcial, ou seja, uma parte quando se compromete e sem pagamento adiantado

### Instâncias spot

Mais barato que a opção *sob demanda*, instâncias spots podem ser retiradas de você pela amazon a qualquer instante, sendo avisado previamente 2 minutos antes de haver desligamento

### Hosts dedicados

Hosts fisicos dedicados para atender a determinados requisitos de conformidade

## Escalabilidade do Amazon EC2

Diz respeito começar apenas com os recursos de que você precisa e projetar sua arquitetura para responder automaticamente às alterações de demanda, fazendo **aumentos** ou **reduções**. 

    Você paga apenas pelos recursos que você usa.

    Você não precisa se preocupar com a falta de capacidade de computação para atender às necessidades de seus clientes
 
 O serviço AWS que fornece essa funcionalidade para instâncias do Amazon EC2 é o **Amazon EC2 Auto Scaling**.

 ![demanda](https://i.ibb.co/g6jfQr2/imagem-2023-08-14-172844669.png)

 ### Amazon EC2 Auto Scaling

 Permite que você adicione ou remova automaticamente instâncias do Amazon EC2 em resposta à alteração da demanda do aplicativo.

 No Amazon EC2 Auto Scaling, há duas abordagens disponíveis: scaling dinâmico e scaling preditivo.

    O scaling dinâmico responde às alterações na demanda. 
    O scaling preditivo programa automaticamente o número correto de instância do Amazon EC2 com base na demanda prevista.

Ao criar um grupo do Auto Scaling, você define o número mínimo de instâncias do Amazon EC2, ou seja, **quantas serão executadas imediatamente após a criação do grupo do Auto Scaling**.

Em seguida,  define-se a capacidade desejada como duas instâncias do Amazon EC2, mesmo que o aplicativo precise de um mínimo de uma única instância do Amazon EC2 para que seja executado.

Porfim, você pode definir a capacidade máxima. Por exemplo, você configura o grupo do Auto Scaling para aumentar em resposta à demanda elevada, mas apenas para um **máximo** de quatro instâncias do Amazon EC2

## Direcionamento de tráfego com o Elastic Load Balancing

O Elastic Load Balancing é o serviço AWS que distribui automaticamente o tráfego de entrada de aplicativos entre vários recursos, como instâncias do Amazon EC2.

    Recebe requisições e encaminha para atingir o processamento nas respectivas instâncias

À medida que você adiciona ou remove instâncias do Amazon EC2 em resposta à quantidade de tráfego de entrada, essas solicitações são direcionadas para o balanceador de carga primeiro. Em seguida, as solicitações se espalham por vários recursos que lidarão com elas.

## Sistema de mensagens e enfileiramento

Arquitetura com acoplamento flexível -> uma única falha não causará falhas em cascata

 

'''
    Amazon Simple Queue Service (Amazon SQS)

    No Amazon SQS, um aplicativo envia mensagens para uma fila. Um usuário ou serviço recupera uma mensagem da fila, processa-a e a exclui da fila.
'''



'''
Amazon Simple Notification Service (Amazon SNS)

Um canal para entrega de mensagens

Exemplo: Newsletter onde os assinantes podem escolher quais tópicos receber notícias
'''

## Outros serviços de computação

O termo “sem servidor” significa que o código é executado em servidores, sem que você precise provisionar ou gerenciar esses servidores. 

A computação sem servidor pode ajustar a capacidade de aplicativos modificando as unidades de consumo, como taxa de transferência e memória.

### AWS Lambda

Serviço que permite a execução de códigos sem a necessidade de provisionar ou gerenciar servidores.

você paga apenas pelo tempo de computação que consumir. As cobranças se aplicam ao tempo em que o código fica em execução.

Por exemplo, uma função simples do Lambda é o redimensionamento automático de imagens com o upload feito na nuvem AWS. Nesse caso, a função é acionada ao fazer upload de uma nova imagem.

Você envia o código para o Lambda -> Define que o código seja acionado a partir de uma origem de evento, como serviços AWS, aplicativos móveis ou endpoints HTTP -> O Lambda executa o código somente quando acionado -> Você paga apenas pelo tempo de computação que usar

### Contêiners

Maneira de empacotar códigos, configurações e dependências do aplicativo em um único objeto

### Amazon Elastic Container Service (Amazon ECS)

Sistema de gerenciamento de contêineres altamente dimensionável e de alto desempenho que permite executar e dimensionar aplicativos em contêineres na AWS.

### Amazon Elastic Kubernetes Service (Amazon EKS)

É um serviço totalmente gerenciado que você pode usar para executar o **Kubernetes** na AWS.

O Kubernetes é um software de código aberto que permite implantar e gerenciar aplicativos em contêineres em grande escala.

### AWS Fargate

Mecanismo de computação sem servidor para contêineres. Com ele, não é necessário provisionar ou gerenciar servidores. O AWS Fargate gerencia sua infraestrutura de servidor para você.


   


