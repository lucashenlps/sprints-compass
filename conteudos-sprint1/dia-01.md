![banner-dia](https://i.ibb.co/yQxFZ38/png-6.png)
### Repositório criado para documentar meus aprendizados durante a primeira Sprint 

# Matriz de Eisenhower
### O que é a Matriz de Eisenhower e como ela pode te ajudar a ser mais eficaz
É uma ferramenta de organização que ajuda com a priorização de tarefas e orientar os processos de tomadas de decisão nas organizações.

Separa as tarefas por **urgência** e **importância**

![Matriz de Einsenhower](https://www.escoladnc.com.br/blog/wp-content/uploads/2022/01/matriz-eisenhower.png)

Tarefas **importântes** são aqueles relacionados ao alcance de determinada meta ou objetivo

Tarefas **urgentes** são aquelas relacionadas ao **prazo** estabelecido para sua realização ou suas consequências para a empresa

Traz benefícios como a priorização de tarefas, melhoria de resultados e tomada clara de decisões.

# Comandos Git
Nessa seção explico comandos principais do Git e suas funcionalidades
## Instalação

É possível baixa-lo clicando [aqui](https://git-scm.com/downloads)!



```bash
git -- version 

#exibe a versão Git instalada em sua máquina. Ideal para verificar se tudo ocorreu corretamente durante a instalação
```
```bash
git add <nome-do-arquivo> || git add . 

#prepara os arquivos para realizar o commit mais adiante.
#o primeiro comando adiciona apenas o que foi especificado, já o segundo adiciona TODOS os arquivos presentes na pasta
```
```bash
git status
#exibe o que será adicionado ou removido ao realizar o commit
```
```bash
git commit -m "mensagem do commit"
#confirma as alterações feitas no projeto
```
```bash
git branch -M "main"
#altera a branch de Master para Main
```
```bash
git remote add origin <link git>
#conecta a pasta que foi realizada o comando git init com o repositório Git 
#só precisa ser executado uma única vez
```
```bash
git push -u origin main
#envia os arquivos ao repositório Git
```
## Versionamento
```bash
git push origin main
#ao adicionar um arquivo a um repositório já existente, não é necessário o " -u "
```
```bash
git checkout -b "nova-implementacao"
#cria a branch nova-implementacao e define ela como branch principal
```
```bash
git checkout main
#retorna para a branch main, caso esteja selecionada outra
```
```bash
#estando na branch main
git merge nova-implementacao
#insere à branch main as atualizações realizadas na branch nova-implementacao
```
## Clonar repositório
```bash
git clone <link git>
#copia o código do repositório para a pasta que foi executado o git init
```
 
 

